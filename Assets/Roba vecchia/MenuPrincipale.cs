﻿using UnityEngine;
using System.Collections;

[SerializeField]
public enum Gusti
{
    Salame,
    Cioccolato
}

public class MenuPrincipale : MonoBehaviour
{
    [Header("Dati",order = 0)]
    public int numeroBello = 10;
    public double n2 = 10;

    [Space(50.0f, order = 1)]
    public float numeroDiCasa = 10;
   
    [Header("Polpette", order = 2)]
    //[HideInInspector]
    public Ricevitore ricevitore;

    [SerializeField]
    int privata = 2;

    public Gusti gustoScelto;

    [Tooltip("Sono il tweener")]

    [TextArea]
    public string testoDaScrivere;


    public TweenColor tweenColor;

    public void RunAnimationClick()
    {
        tweenColor.to = Color.green;
        //tweenColor.style = UITweener.Style.Loop;
        tweenColor.PlayForward();

        // adds the OnAnimationFinish function
        //EventDelegate mioEventoBellissimo = new EventDelegate(this, "OnAnimationFinishParametrizzata");
        EventDelegate mioEventoBellissimo = new EventDelegate(ricevitore, "RiceviNumero");
        mioEventoBellissimo.parameters[0].value = 100;

        EventDelegate del2 = new EventDelegate(ricevitore, "RiceviNumero");
        del2.parameters[0].value = 150;
        //EventDelegate.Set(tweenColor.onFinished, OnAnimationFinish);
        //EventDelegate.Set(tweenColor.onFinished, mioEventoBellissimo);
        
        //EventDelegate.Add(tweenColor.onFinished, mioEventoBellissimo);
        //EventDelegate.Add(tweenColor.onFinished, del2);
        
        tweenColor.onFinished.Add(mioEventoBellissimo);
        tweenColor.onFinished.Add(del2);
    }


    public UIPopupList lstLanguages;

    void Start()
    {
        lstLanguages.Set("Italiano");
        //Localization.language = "Italiano";
    }

    public void InitClick()
    {
        print("Sto iniziando");
    }

    public void PauseClick()
    {
        Debug.Break();
    }

    public void ExitClick()
    {
        Application.Quit();
    }

    public void OnLanguageChange()
    {
        print("Valore scelto: " + UIPopupList.current.value);
        //Localization.language = UIPopupList.current.value;
    }

    public void OnAnimationFinishParametrizzata(int numero)
    {
        print("Cose");
        print("Animazione finita, stampo: " + numero);
    }

    public void OnAnimationFinish()
    {
        print("Animazione finita");
    }
}
