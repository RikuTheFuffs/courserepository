﻿/* ----------------------------------------------
 * 
 * 				Athlon Hunters Client
 * 
 * Original Author: Abela Paolo
 * Creation Date: 10 October 2015
 * Updates: 
 * 
 * [1] 12 October 2015 (Paolo Abela) --> - added WriteJSONToFile()
 * 
 * Copyright © 2015-2016 Starwork Game Creators
 * ----------------------------------------------
 */

using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.IO;

/// <summary>
/// Utility class for JSON files
/// </summary>

public class JSONUtilities : MonoBehaviour
{

    /// <summary>
    /// Reads the content of a JSON file and returns it
    /// </summary>
    /// <param name="filePath"> path of the JSON file</param>
    /// <returns>the encoded JSON if the reading was successfull, a "Status : failed" JSON otherwise</returns>

    public static JSONNode ReadJSONFromFile(string filePath)
    {
        StreamReader reader = null;
        JSONNode finalJSON = JSONNode.Parse("{ \"Status\" : \"FAILED\" }");
        try
        {
            reader = new StreamReader(filePath);
            finalJSON = JSONNode.Parse(reader.ReadToEnd());
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        finally
        {
            if (reader != null)
            {
                reader.Close();
                reader.Dispose();
            }
        }
        return finalJSON;
    }

    /// <summary>
    /// Writes the content of a JSON in a file
    /// </summary>
    /// <param name="filePath"> path of the JSON file</param>
    /// <param name="content">content of the JSON that you want to write</param>
    /// <param name="singleLine">Should the JSON be written without spaces?</param>
    /// <param name="append">Should the original file be completeley rewritten? (if exists)</param>

    public static void WriteJSONToFile(string filePath, JSONNode content, bool singleLine = false, bool append = false)
    {
        StreamWriter writer = null;
        try
        {
            writer = new StreamWriter(filePath, append);
            if (singleLine)
            {
                writer.WriteLine(content.ToString());
            }
            else
            {
                writer.WriteLine(content.ToString(""));
            }
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        finally
        {
            if (writer != null)
            {
                writer.Close();
                writer.Dispose();
            }
        }
    }
}
