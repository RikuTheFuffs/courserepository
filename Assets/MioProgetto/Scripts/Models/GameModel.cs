﻿using UnityEngine;
using System.Collections;
using thelab.mvc;

public class GameModel : Model<GameApplication>
{
    public MatchModel matchModel
    {
        get { return myMatchModel = Assert<MatchModel>(myMatchModel);  }
    }
    MatchModel myMatchModel = null;

}
