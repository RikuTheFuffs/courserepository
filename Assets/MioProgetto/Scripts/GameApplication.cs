﻿using UnityEngine;
using System.Collections;
using thelab.mvc;

public class GameApplication : BaseApplication<GameModel,GameView,GameController>
{

    /// <summary>
    /// Singleton of the application
    /// </summary>
    public static GameApplication instance = null;

    protected override void Awake()
    {
        base.Awake();
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Notify(string evento, params object[] parametri)
    {
        app.Notify(evento, this, parametri);
    }
}
