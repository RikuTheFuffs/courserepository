﻿using UnityEngine;
using System.Collections;

public class MenuNotifications : MonoBehaviour
{
    public const string start = "mainmenu.start";
    public const string options = "mainmenu.options";
    public const string exit = "mainmenu.exit";
    public const string getBackToTheMenu = "mainmenu.returnHere";

}
