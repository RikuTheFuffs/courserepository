﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Abela Paolo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � 2016 Starwork Game Creators
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using thelab.mvc;
using SimpleJSON;

///<summary>
///
///</summary>

public class MainMenuController : Controller<GameApplication>
{
    MainMenuView mainMenuView
    {
        get { return app.view.mainMenuView; }
    }

    void Start()
    {
        JSONNode mioJSON = JSONNode.Parse("{}");

        //cfg di quando sono entrato nel menù
        JSONNode configurazioneIniziale = JSONNode.Parse("{}");
        configurazioneIniziale["nome"] = "paolo";

        //cfg modificata al momento ma non ancora salvata
        JSONNode configurazioneAttuale = JSONNode.Parse(configurazioneIniziale.ToString());
        //JSONNode configurazioneAttuale = configurazioneIniziale; --> wrong!
        configurazioneAttuale["nome"] = "filippo";

        print("Iniziale: " + configurazioneIniziale.ToString());
        print("Modificata: " + configurazioneAttuale.ToString());

        /*

        //mioJSON.Add("nome", "paolo");
        mioJSON["nome"] = "paolo";
        mioJSON["maggiorenne"] = "true";

        print("il mio nome è : " + mioJSON["nome"]);

        if(mioJSON["maggiorenne"].AsBool)
        {
            print("Maggiorenne! :D");

        }

        if (mioJSON["nome"].Value == "paolo")
        {
            print("Funziona");
        }
        else
        {
            print(" :( ");
        }
        */
        //print(mioJSON.ToString(""));
    }

    public override void OnNotification(string p_event, Object p_target, params object[] p_data)
    {
        switch(p_event)
        {
            case MenuNotifications.start:
                print("Click sul pulsante start");

                Enable(false);

                //faccio apparire la schermata di caricamento

                Notify(GameNotifications.start);

                break;

            case MenuNotifications.getBackToTheMenu:

                Enable(true);

                //create an empty json object
                


                break;

            case MenuNotifications.options:
                print("Click sul pulsante Options");
                break;

            case MenuNotifications.exit:
                print("Click sul pulsante Exit");
                break;

            case "eventoDaComponents":

                int numero = (int)(p_data[0]);
                print("Ho ricevuto dal components il numero: " + numero);

                break;
        }
    }

    void Enable(bool shouldEnable)
    {
        if(shouldEnable)
        {
            //eseguo la logica per ATTIVARE la view
            //es --> ri-attivare le animazioni
            // --> resetto posizioni, etc...
            
        }
        else
        {
            //eseguo la logica per disattivare la view
            //es --> disattivo le animazioni
            // --> resetto posizioni, etc...
        }
        mainMenuView.gameObject.SetActive(shouldEnable);
    }
}
