﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Abela Paolo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � 2016 Starwork Game Creators
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using thelab.mvc;

///<summary>
///
///</summary>

public class MatchController : Controller<GameApplication>
{
    MatchModel matchModel { get { return app.model.matchModel; } }



    float startTime = 0.0f; //when did the game start?
    float endTime = 0.0f; //when did the game finish?

    void Start()
    {
        //matchModel.highscores["punteggio"] = "9999";
        matchModel.highscores = JSONUtilities.ReadJSONFromFile(Application.streamingAssetsPath + "/punteggio.txt");
        print("Ho letto il file degli highscores: " + matchModel.highscores.ToString());

    }


    public override void OnNotification(string p_event, Object p_target, params object[] p_data)
    {
        switch (p_event)
        {
            case GameNotifications.start:
                print("Inizia partita");
                GestoreLivelli.instance.Inizializza(matchModel.firstLevel);
                StartTimer();


                

                /* 
                - re-inizializzo il livello:
                    - disattivare/distruggere gli oggetti che appaiono successivamente
                    - attivare gli oggetti iniziali del livello
                */
                
                //spawno il giocatore da qualche parte
                //riavvio l'orologio di gioco
                //---  avvio di altre meccaniche

                break;

            case GameNotifications.end:

                //metto in pausa il livello
                //uccido il giocatore (+ i suoi script)
                //fermo l'orologio di gioco
                //--- stop di altre meccaniche

                StopTimer();
                SaveScore();
                GestoreLivelli.instance.ResetCurrentLevel();
                print("torno al menù principale");
                Notify(MenuNotifications.getBackToTheMenu);


                break;
        }
    }

    void StartTimer()
    {
        startTime = Time.time; //-->  60
    }
    void StopTimer()
    {
        endTime = Time.time; //--> 90
    }

    void SaveScore()
    {
        float score = endTime - startTime;
        print("Score: " + score);
        
        //salviamo su file

        if(matchModel.highscores["punteggio"].AsFloat > score)
        {
            matchModel.highscores["punteggio"] = "" + score;

            JSONUtilities.WriteJSONToFile(Application.streamingAssetsPath + "/punteggio.txt", matchModel.highscores);

        }
        else
        {
            print("non sei stato abbastanza bravo, bidone");
        }
    }

}
