﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abilita o disabilita alcuni oggetti della scena
/// </summary>

public class ObjectEnabler : MonoBehaviour
{
    int a;
    string b;

    public void Enable(GameObject target, bool enable)
    {
        target.SetActive(enable);
    }

    void Start()
    {
       /* print(GameApplication.instance.gameObject.name);
        print("View più importante: " + GameApplication.instance.view.gameObject.name);
        print("Menu view:  " + GameApplication.instance.view.mainMenuView.gameObject.name);
        GameApplication.instance.Notify("eventoDaComponents", 100);*/
    }
}
