﻿using UnityEngine;
using System.Collections;

public class Arrivo : MonoBehaviour
{

	void OnTriggerEnter(Collider col)
    {
        print("L'oggetto: " + col.gameObject.name + " mi ha raggiunto");
        GameApplication.instance.Notify(GameNotifications.end);

    }
}
