﻿using UnityEngine;
using System.Collections;

public class GestoreLivelli : MonoBehaviour
{
    public static GestoreLivelli instance = null;

    public GameObject giocatore;
    public GameObject spawnPoint;
    GameObject currentLevel = null;
    GameObject currentPlayer = null;
    
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Inizializza(GameObject livello)
    {
        currentLevel = livello;
        livello.SetActive(true);
        SpawnaGiocatore(spawnPoint);
    }

    void SpawnaGiocatore(GameObject sp)
    {
        print("Spawno il giocatore");
        currentPlayer = Instantiate(giocatore, sp.transform.position, sp.transform.rotation) as GameObject;
        print("Nome giocatore: " + currentPlayer.name);
    }

    public void ResetCurrentLevel()
    {
        ChiudiLivelloCorrente();
        UccidiGiocatoreCorrente();
    }

    void ChiudiLivelloCorrente()
    {
        currentLevel.SetActive(false);
    }

    void UccidiGiocatoreCorrente()
    {
        Destroy(currentPlayer);
    }

}
