﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Abela Paolo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � 2016 Starwork Game Creators
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using thelab.mvc;

///<summary>
///
///</summary>

public class GameView : View<GameApplication>
{
    public MainMenuView mainMenuView
    {
        get { return myMainMenuView = Assert<MainMenuView>(myMainMenuView); }
    }

    MainMenuView myMainMenuView;
}
