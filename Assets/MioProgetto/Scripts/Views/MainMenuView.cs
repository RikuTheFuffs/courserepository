﻿using UnityEngine;
using System.Collections;
using thelab.mvc;

public class MainMenuView : View<GameApplication>
{
    /// <summary>
    /// Label that contains the title of the game
    /// </summary>

    [SerializeField]
    UILabel lblGameTitle;

    [SerializeField]
    UIButton btnStart;

    [SerializeField]
    UIButton btnOptions;

    [SerializeField]
    UIButton btnExit;

    void Awake()
    {
        EventDelegate startDelegate = new EventDelegate(StartClick);
        btnStart.onClick.Add(startDelegate);

        EventDelegate optionsDelegate = new EventDelegate(OptionsClick);
        btnOptions.onClick.Add(optionsDelegate);

        EventDelegate exitDelegate = new EventDelegate(ExitClick);
        btnExit.onClick.Add(exitDelegate);
    }

    void StartClick()
    {
        Notify(MenuNotifications.start);
    }

    void OptionsClick()
    {
        Notify(MenuNotifications.options);

    }

    void ExitClick()
    {
        Notify(MenuNotifications.exit);

    }

    //commento
}
